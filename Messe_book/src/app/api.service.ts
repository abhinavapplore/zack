import {
  Injectable,
  EventEmitter,
  Output
} from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, LoadingController } from '@ionic/angular';




@Injectable({
  providedIn: 'root'
})
export class ApiService {
  @Output() devloperevent = new EventEmitter < any > ();
  rootUrl;
  load: any;
  constructor(public storage:Storage, public navCtrl:NavController, public loadingController:LoadingController ) { }


  Navigateroot(x) {
    var route = '/' + x
    this.navCtrl.navigateRoot(route)
  }

  Navigate(x) {
    var route = '/' + x
    this.navCtrl.navigateForward(route)
  }

  Navigateback(x) {
    var route = '/' + x
    this.navCtrl.navigateBack(route)
  }


  async loading(msg) {
    this.load = true;
    return await this.loadingController.create({
      message: msg,
          spinner: 'dots',
      // duration: 5000,
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.load) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismissLoading() {
    this.load = false;
    return await this.loadingController.dismiss().then(() => console.log('dismissed'));
  }

  getPinnwand(){
    this.storage.get('pinnwand').then((pinnwand)=>{
     
    });
  }


}
