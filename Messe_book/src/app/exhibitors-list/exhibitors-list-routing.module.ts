import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExhibitorsListPage } from './exhibitors-list.page';

const routes: Routes = [
  {
    path: '',
    component: ExhibitorsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExhibitorsListPageRoutingModule {}
