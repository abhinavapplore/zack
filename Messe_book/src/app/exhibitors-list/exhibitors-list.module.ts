import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExhibitorsListPageRoutingModule } from './exhibitors-list-routing.module';

import { ExhibitorsListPage } from './exhibitors-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExhibitorsListPageRoutingModule
  ],
  declarations: [ExhibitorsListPage]
})
export class ExhibitorsListPageModule {}
