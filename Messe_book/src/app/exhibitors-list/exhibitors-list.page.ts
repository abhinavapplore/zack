import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exhibitors-list',
  templateUrl: './exhibitors-list.page.html',
  styleUrls: ['./exhibitors-list.page.scss'],
})
export class ExhibitorsListPage implements OnInit {

  alle:boolean=true;
  besucht:boolean=false;
  nochmalHingehen:boolean=false;

  constructor() { }

  ngOnInit() {
  }

  Alle(){
    this.alle=true;
    this.besucht=false;
    this.nochmalHingehen=false;
  }

  Besucht(){
    this.alle=false;
    this.besucht=true;
    this.nochmalHingehen=false;
  }

  NochmalHingehen(){
    this.alle=false;
    this.besucht=false;
    this.nochmalHingehen=true;
  }

}
