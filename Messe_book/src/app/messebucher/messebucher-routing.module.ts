import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MessebucherPage } from './messebucher.page';

const routes: Routes = [
  {
    path: '',
    component: MessebucherPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MessebucherPageRoutingModule {}
