import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MessebucherPageRoutingModule } from './messebucher-routing.module';

import { MessebucherPage } from './messebucher.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MessebucherPageRoutingModule
  ],
  declarations: [MessebucherPage]
})
export class MessebucherPageModule {}
