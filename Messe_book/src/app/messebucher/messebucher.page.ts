import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-messebucher',
  templateUrl: './messebucher.page.html',
  styleUrls: ['./messebucher.page.scss'],
})
export class MessebucherPage implements OnInit {

  constructor(public route: Router) { }

  ngOnInit() {
  }

  neues(){
    this.route.navigateByUrl('messebucher1');
  }

}
