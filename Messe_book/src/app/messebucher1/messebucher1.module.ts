import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Messebucher1PageRoutingModule } from './messebucher1-routing.module';

import { Messebucher1Page } from './messebucher1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Messebucher1PageRoutingModule
  ],
  declarations: [Messebucher1Page]
})
export class Messebucher1PageModule {}
