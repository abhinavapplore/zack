import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-messebucher1',
  templateUrl: './messebucher1.page.html',
  styleUrls: ['./messebucher1.page.scss'],
})
export class Messebucher1Page implements OnInit {

  constructor(public route: Router) { }

  ngOnInit() {
  }

  back(){
    this.route.navigateByUrl('messebucher');
  }

  exhibtioner(){
    this.route.navigateByUrl('exhibtioner1');
  }

}
