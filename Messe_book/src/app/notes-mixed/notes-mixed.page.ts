import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notes-mixed',
  templateUrl: './notes-mixed.page.html',
  styleUrls: ['./notes-mixed.page.scss'],
})
export class NotesMixedPage implements OnInit {
  isAll:boolean=true;
  isNotizen:boolean;
  isAudio:boolean;
 
  isArticle:boolean=false;
  constructor(public router: Router) { }

  ngOnInit() {
  }

  showAudio(){
    if(this.isAudio){
      this.isAudio=false;
      this.isAll=true;
      
    }else{
      this.isAll=false;
      this.isNotizen=false;
      this.isArticle=false;
      this.isAudio=true;
    }
   
  }

  showNotizen(){
    if(this.isNotizen){
      this.isNotizen=false;
      this.isAll=true;
      console.log('heyyyy')
    }else{
      this.isAll=false;
      this.isArticle=false;
      this.isAudio=false;
      this.isNotizen=true;
      console.log('heyyyy')
    }
  }

  showArticle(){
    if(this.isArticle){
      this.isArticle=false;
      this.isAll=true;
      
    }else{
      this.isAll=false;
      this.isNotizen=false;
      this.isAudio=false;
      this.isArticle=true;
    }
   
  }

  showCamera(){
    if(this.isAll){
      this.isAll=true;
      this.isNotizen=false;
      this.isArticle=false;
      this.isAudio=false;
      
    }else{
      this.isAll=true;
      this.isNotizen=false;
      this.isArticle=false;
      this.isAudio=false;
    }
   
  }

  addNotizen(){
    this.router.navigateByUrl("tabs/productadd");
  }

  LISTENANSICHT(){
    this.router.navigateByUrl("tabs/exhibitors-list");
  }

}
