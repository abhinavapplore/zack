import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OptionsmodalPage } from './optionsmodal.page';

const routes: Routes = [
  {
    path: '',
    component: OptionsmodalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OptionsmodalPageRoutingModule {}
