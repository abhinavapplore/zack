import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OptionsmodalPageRoutingModule } from './optionsmodal-routing.module';

import { OptionsmodalPage } from './optionsmodal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OptionsmodalPageRoutingModule
  ],
  declarations: [OptionsmodalPage]
})
export class OptionsmodalPageModule {}
