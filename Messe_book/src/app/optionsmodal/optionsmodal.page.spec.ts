import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OptionsmodalPage } from './optionsmodal.page';

describe('OptionsmodalPage', () => {
  let component: OptionsmodalPage;
  let fixture: ComponentFixture<OptionsmodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionsmodalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OptionsmodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
