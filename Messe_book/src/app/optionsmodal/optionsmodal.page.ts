import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-optionsmodal',
  templateUrl: './optionsmodal.page.html',
  styleUrls: ['./optionsmodal.page.scss'],
})
export class OptionsmodalPage implements OnInit {

  constructor(public modalCtrl:ModalController) { }

  ngOnInit() {
  }


  dismiss(data?){
    this.modalCtrl.dismiss(data);
  }

}
