import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddAudioNotePagePageRoutingModule } from './add-audio-note-page-routing.module';

import { AddAudioNotePagePage } from './add-audio-note-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddAudioNotePagePageRoutingModule
  ],
  declarations: [AddAudioNotePagePage]
})
export class AddAudioNotePagePageModule {}
