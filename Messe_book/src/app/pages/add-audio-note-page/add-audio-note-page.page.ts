import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-audio-note-page',
  templateUrl: './add-audio-note-page.page.html',
  styleUrls: ['./add-audio-note-page.page.scss'],
})
export class AddAudioNotePagePage implements OnInit {
  recording:boolean=false;
  recorded:boolean=false;

  constructor(private route: Router) { }


  ngOnInit() {
  }


  return(){
    this.route.navigate(['add-note-page'])
  }

  startRecording(){
    this.recording=true;
  }

  

  go(){
    console.log(this.recorded);
    if(this.recorded==true){
      this.route.navigate(['tabs/katalog'])
    }else{
      this.recorded=true;
      this.recording=false;
      
    }
   
  }

 

}
