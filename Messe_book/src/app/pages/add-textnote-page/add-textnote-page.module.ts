import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddTextnotePagePageRoutingModule } from './add-textnote-page-routing.module';

import { AddTextnotePagePage } from './add-textnote-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddTextnotePagePageRoutingModule
  ],
  declarations: [AddTextnotePagePage]
})
export class AddTextnotePagePageModule {}
