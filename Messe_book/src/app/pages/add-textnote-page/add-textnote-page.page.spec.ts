import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddTextnotePagePage } from './add-textnote-page.page';

describe('AddTextnotePagePage', () => {
  let component: AddTextnotePagePage;
  let fixture: ComponentFixture<AddTextnotePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTextnotePagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddTextnotePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
