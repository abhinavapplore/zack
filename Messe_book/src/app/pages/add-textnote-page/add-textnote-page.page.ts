import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ApiService } from 'src/app/api.service';


@Component({
  selector: 'app-add-textnote-page',
  templateUrl: './add-textnote-page.page.html',
  styleUrls: ['./add-textnote-page.page.scss'],
})
export class AddTextnotePagePage implements OnInit {
  description;
  title;
  category;
  isPinwand:boolean;
  pinwandArray:any=[];
  constructor(private route: Router, public storage:Storage,public api:ApiService) { }

  return(){
    console.log("HEY")
    this.route.navigate(['add-note-page'])
  }


  ionViewDidEnter() {
    this.storage.get('pinnwand').then((pinnwand)=>{
      this.pinwandArray=pinnwand;
      if(this.pinwandArray==null||this.pinwandArray==undefined||this.pinwandArray.length==0){
       this.isPinwand=false;
       }else{
         this.isPinwand=true;
       }
       
     });
    
  }

  ngOnInit(){
    
  }


  add(){


   var textNote ={
      category:'textNote',
      title:this.title,
      description:this.description
    }
    if(this.isPinwand){
      this.pinwandArray.push(textNote);
      this.storage.set('pinnwand',this.pinwandArray);
      console.log(this.pinwandArray);
      this.api.Navigate('tabs/katalog');
    }else{
      this.pinwandArray=[];
      this.pinwandArray.push(textNote);
      this.storage.set('pinnwand',this.pinwandArray);
      this.api.Navigate('tabs/katalog');
    }

  }

}
