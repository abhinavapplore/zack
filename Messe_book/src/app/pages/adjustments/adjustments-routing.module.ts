import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdjustmentsPage } from './adjustments.page';

const routes: Routes = [
  {
    path: '',
    component: AdjustmentsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdjustmentsPageRoutingModule {}
