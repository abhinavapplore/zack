import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Audionote3Page } from './audionote3.page';

const routes: Routes = [
  {
    path: '',
    component: Audionote3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Audionote3PageRoutingModule {}
