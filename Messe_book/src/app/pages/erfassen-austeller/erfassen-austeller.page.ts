import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-erfassen-austeller',
  templateUrl: './erfassen-austeller.page.html',
  styleUrls: ['./erfassen-austeller.page.scss'],
})
export class ErfassenAustellerPage implements OnInit {

  constructor(public api:ApiService) { }


  ngOnInit() {
  }

  goToKatalog(){
    this.api.Navigate('tabs/katalog');
  }
}
