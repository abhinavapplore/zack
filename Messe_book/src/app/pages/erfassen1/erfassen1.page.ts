import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-erfassen1',
  templateUrl: './erfassen1.page.html',
  styleUrls: ['./erfassen1.page.scss'],
})
export class Erfassen1Page implements OnInit {

  constructor(private route: Router, public api:ApiService) { }


  goAussteller(){
    this.api.Navigate(['tabs/erfassen-austeller'])
  }

  goAddNote(){
    this.api.Navigate(['add-note-page'])
  }

  ngOnInit() {
  }

  goToerfassen2(){
this.api.Navigate('tabs/erfassen2');
  }

}
