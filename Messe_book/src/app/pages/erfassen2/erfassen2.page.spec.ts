import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Erfassen2Page } from './erfassen2.page';

describe('Erfassen2Page', () => {
  let component: Erfassen2Page;
  let fixture: ComponentFixture<Erfassen2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Erfassen2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Erfassen2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
