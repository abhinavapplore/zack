import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExhibitionSelectionPage } from './exhibition-selection.page';

const routes: Routes = [
  {
    path: '',
    component: ExhibitionSelectionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExhibitionSelectionPageRoutingModule {}
