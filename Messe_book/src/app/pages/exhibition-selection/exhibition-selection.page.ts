import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-exhibition-selection',
  templateUrl: './exhibition-selection.page.html',
  styleUrls: ['./exhibition-selection.page.scss'],
})
export class ExhibitionSelectionPage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    slidesPerView:2.7,
    spaceBetween:10
  };
  constructor(public router:Router,public api:ApiService) { }
 
  ngOnInit() {
  }

  austeller(){
    this.api.Navigate('tabs/exhibitors-list');
  }

}
