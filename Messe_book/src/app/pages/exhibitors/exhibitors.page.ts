import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-exhibitors',
  templateUrl: './exhibitors.page.html',
  styleUrls: ['./exhibitors.page.scss'],
})
export class ExhibitorsPage implements OnInit {

  constructor(private route: Router) { }

  go(){
    this.route.navigate(['bl-katalog'])
  }

  ngOnInit() {
  }

}
 