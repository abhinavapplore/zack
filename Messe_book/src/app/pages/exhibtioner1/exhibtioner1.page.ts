import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-exhibtioner1',
  templateUrl: './exhibtioner1.page.html',
  styleUrls: ['./exhibtioner1.page.scss'],
})
export class Exhibtioner1Page implements OnInit {

  previousIndex=0;

  array=[{"title":"Aktuell","text1":"Frankfurter Buchmesse","text2":"Leipziger Buchmesse",
  "text3":"Messe 3","isSelected":true},
  {"title":"Zukunft","text1":"Frankfurter Buchmesse","text2":"Leipziger Buchmesse",
  "text3":"Messe 3","isSelected":false},
  {"title":"Vergangenheit","text1":"Frankfurter Buchmesse","text2":"Leipziger Buchmesse",
  "text3":"Messe 3","isSelected":false}]

  constructor(private route: Router,public navCtrl:NavController) { }


  ngOnInit() {
  }

  select(i){
    this.array[this.previousIndex].isSelected=false;
    this.array[i].isSelected=true;
    this.previousIndex=i;
  }

  goNext(){
    this.navCtrl.navigateRoot('tabs/katalog')
  }


  nextPage(){
    this.route.navigateByUrl("tabs/notes-mixed")
  }

}

