import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { SocialSharingPage } from '../../social-sharing/social-sharing.page';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-katalog',
  templateUrl: './katalog.page.html',
  styleUrls: ['./katalog.page.scss'],
})
export class KatalogPage implements OnInit {
  pinwandArray:any=[];
  isPinwand:boolean=false;
  isAll:boolean=true;
  isNotizen:boolean=true;
  isAudio:boolean;
  isArticle:boolean=false;
  isCamera:boolean=false;

  slideOpts={
    initialSlide: 0,
    slidesPerView:2.5
  }

  slideOpts2={
    initialSlide: 0,
    slidesPerView:1
  }

  constructor(private route: Router, public api:ApiService,private socialSharing: SocialSharing,
     public storage:Storage,public modalController: ModalController) { }
  return(){
    this.route.navigate(['add-audio-note-page'])
  }

  ngOnInit() {
   
    this.storage.get('pinnwand').then((pinnwand)=>{
     this.pinwandArray=pinnwand;
     console.log(this.pinwandArray);
     if(this.pinwandArray==null||this.pinwandArray==undefined||this.pinwandArray.length==0){
      this.isPinwand=false;
      }else{
        this.isPinwand=true;
      }
      
    });
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: SocialSharingPage,
      cssClass: 'socialSharingModal'
    });
    return await modal.present();
  }


  addPinnwand(){
    console.log("hey");
    this.api.Navigate('tabs/add-page');

  }
  
  LISTENANSICHT(){
    this.route.navigateByUrl("tabs/exhibitors-list");
  }

  showAudio(){
    if(this.isAudio){
      this.isAudio=false;
      this.isAll=true;
      
    }else{
      this.isAll=false;
      this.isNotizen=false;
      this.isArticle=false;
      this.isCamera=false;
      this.isAudio=true;
    }
   
  }

  showNotizen(){
    if(this.isCamera){
      this.isNotizen=false;
      this.isAll=true;
      console.log('heyyyy')
    }else{
      this.isAll=false;
      this.isArticle=false;
      this.isAudio=false;
      this.isNotizen=false;
      this.isCamera=true;
      console.log('heyyyy')
    }
  }

  showArticle(){
    if(this.isArticle){
      this.isArticle=false;
      this.isAll=true;
      
    }else{
      this.isAll=false;
      this.isNotizen=false;
      this.isAudio=false;
      this.isCamera=false;
      this.isArticle=true;
    }
   
  }

  showCamera(){
    if(this.isNotizen){
      // this.isNotizen=false;
      // this.isAll=true;
      this.isAll=true;
      this.isNotizen=false;
      this.isArticle=false;
      this.isAudio=false;
      
    }else{
      this.isAll=true;
      this.isNotizen=false;
      this.isArticle=false;
      this.isAudio=false;
      // this.isNotizen=true;
    }
   
  }

  addNotizen(){
    this.route.navigateByUrl("tabs/productadd");
  }

  

  anfragen(){
    this.socialSharing.canShareViaEmail().then((res) => {
      console.log("Sharing via email is possible");
      console.log(res);
      if(res){
        this.socialSharing.shareViaEmail('Hey Zack', 'Testing email', ['abhishek811997gmail.com']).then(() => {
          // Success!
        }).catch(() => {
          // Error!
        });
      }      
    }).catch(() => {
      console.log("Sharing via email is not possible");
    });
  }

}
