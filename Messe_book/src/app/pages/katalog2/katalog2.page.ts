import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-katalog2',
  templateUrl: './katalog2.page.html',
  styleUrls: ['./katalog2.page.scss'],
})
export class Katalog2Page implements OnInit {

  constructor(public route: Router) { }

  ngOnInit() {
  }

  LISTENANSICHT(){
    this.route.navigateByUrl("tabs/exhibitors-list");
  }

}
