import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GooglePlus } from '@ionic-native/google-plus/ngx';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private route: Router,private googlePlus: GooglePlus) {}

  go2() {
    this.route.navigate(["signup"])
  }

  

  goNext(){
    this.route.navigateByUrl('tabs/katalog');
   
  }

  loginWithGoogle(){
    this.googlePlus.login({})
    .then(res => console.log(res))
    .catch(err => console.error(err));
  }


ngOnInit() {
  }
}
