import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NoteSelPage } from './note-sel.page';

describe('NoteSelPage', () => {
  let component: NoteSelPage;
  let fixture: ComponentFixture<NoteSelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteSelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NoteSelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
