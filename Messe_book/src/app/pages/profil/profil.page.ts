import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.page.html',
  styleUrls: ['./profil.page.scss'],
})
export class ProfilPage implements OnInit {
  FIRMA ="Firmenname";
  VORNAME ="Max";
  NACHNAME = "Mustermann";
  email = "m.mustermann@mail.com";
  PASSWORT = "*********";
  PASSWORT2 ="******";

  constructor(private route: Router) { }

  go() {
    this.route.navigate(['login'])
  }

  ngOnInit() {
  }

}
