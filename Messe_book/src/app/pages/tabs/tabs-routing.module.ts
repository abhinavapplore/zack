import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
     
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../exhibition-selection/exhibition-selection.module').then(m => m.ExhibitionSelectionPageModule)
          }
        ]
      }, {
        path: 'erfassen1',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../erfassen1/erfassen1.module').then(m => m.Erfassen1PageModule)
          }
        ]
      },
      {
        path: 'erfassen2',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../erfassen2/erfassen2.module').then(m => m.Erfassen2PageModule)
          }
        ]
      },{
        path: 'anfragen',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../anfragen-seite/anfragen-seite.module').then(m => m.AnfragenSeitePageModule)
          }
        ]
      },
      
      {
        path: '',
        children: [
          {
            path: 'erfassen-austeller',
            loadChildren: () =>
              import('../erfassen-austeller/erfassen-austeller.module').then(m => m.ErfassenAustellerPageModule)
          }
        ]
      },
      {
        path: '',
        children: [
          {
            path: 'katalog',
            loadChildren: () =>
              import('../katalog/katalog.module').then(m => m.KatalogPageModule)
          }
        ]
      },{
        path: '',
        children: [
          {
            path: 'adjustments',
            loadChildren: () =>
              import('../adjustments/adjustments.module').then(m => m.AdjustmentsPageModule)
          }
        ]
      },

      {
        path: '',
        children: [
          {
            path: 'add-note-page',
            loadChildren: () => import('../add-note-page/add-note-page.module').then( m => m.AddNotePagePageModule)
          }
        ]
      
      },
      {
        path: '',
        children: [
          {
            path: 'profil',
            loadChildren: () => import('../profil/profil.module').then( m => m.ProfilPageModule)
          }
        ]
      
      },
      
      {
        path: '',
        children: [
          {
            path: 'add-textnote-page',
            loadChildren: () => import('../add-textnote-page/add-textnote-page.module').then( m => m.AddTextnotePagePageModule)
          }
        ]
       
      },
      {
        path: '',
        children: [
          {
            path: 'add-audio-note-page',
            loadChildren: () => import('../add-audio-note-page/add-audio-note-page.module').then( m => m.AddAudioNotePagePageModule)
          }
          
        ]
       
      },{
        path: '',
        children: [
          {
            path: 'add-page',
            loadChildren: () => import('../add-page/add-page.module').then( m => m.AddPagePageModule)
          }
          
        ]
       
      },{
        path: '',
        children: [
          {
            path: 'add-product',
            loadChildren: () => import('../add-product/add-product.module').then( m => m.AddProductPageModule)
          }
          
        ]
       
      },
      {
        path: '',
        children: [
          {
            path: 'exhibitors-list',
            loadChildren: () => import('../../exhibitors-list/exhibitors-list.module').then( m => m.ExhibitorsListPageModule)
          }
          
        ]
       
      },{
        path: '',
        children: [
          {
            path: 'notes-mixed',
            loadChildren: () => import('../../notes-mixed/notes-mixed.module').then( m => m.NotesMixedPageModule)
          }
          
        ]
       
      },{
        path: '',
        children: [
          {
            path: 'productadd',
            loadChildren: () => import('../../productadd/productadd.module').then( m => m.ProductaddPageModule)
          }
          
        ]
       
      },
      
      
      
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}


