import { Component } from '@angular/core';

import { ModalController, NavController } from '@ionic/angular';
import{ OptionsmodalPage } from '../../optionsmodal/optionsmodal.page'
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  
  constructor(public modalController: ModalController, public navCtrl:NavController,
    public router:Router) {}

  async presentModal() {
    const modal = await this.modalController.create({
      component: OptionsmodalPage,
      cssClass: 'myOptionsmodalPage'
    });
     await modal.present();
    const { data } = await modal.onWillDismiss();
    if(data){
      console.log(data);
      if(data=="messe"){
        this.navCtrl.navigateRoot('exhibtioner1');
      }else if(data=="profil"){
        this.router.navigateByUrl('tabs/profil');
      }else if(data=="einstel"){
        this.router.navigateByUrl('tabs/adjustments');
      }else {
        this.navCtrl.navigateRoot('login');
      }
    }else{
     
    }

  }

  goToErfassen(){
    this.router.navigateByUrl('tabs/erfassen1');
  }

  goToHome(){
    this.router.navigateByUrl('tabs/home');
  }

  goToExhibitorsList(){
    this.router.navigateByUrl('tabs/exhibitors-list');
  }

  goToDashboard(){
    this.router.navigateByUrl('tabs/katalog');
  }

}
