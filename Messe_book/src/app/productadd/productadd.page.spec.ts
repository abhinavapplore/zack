import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductaddPage } from './productadd.page';

describe('ProductaddPage', () => {
  let component: ProductaddPage;
  let fixture: ComponentFixture<ProductaddPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductaddPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductaddPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
