import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-productadd',
  templateUrl: './productadd.page.html',
  styleUrls: ['./productadd.page.scss'],
})
export class ProductaddPage implements OnInit {

  constructor(public route:Router) { }

  ngOnInit() {
  }

  return(){
    this.route.navigate(['tabs/katalog'])
  }

  goTextnote(){
    
    this.route.navigate(['tabs/add-textnote-page'])
  }

  goAudionote(){
  
    this.route.navigate(['tabs/add-audio-note-page'])
  }


 

}
